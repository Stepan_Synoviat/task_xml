package xml.parsers.sax;

import org.xml.sax.SAXException;
import xml.model.Plane;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SAXParserUser {
	private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

	public static List<Plane> parsePlases(File xml, File xsd){
		List<Plane> planesList = new ArrayList<>();
		try {
			saxParserFactory.setSchema(SAXValidator.createSchema(xsd));

			SAXParser saxParser = saxParserFactory.newSAXParser();
			SAXHandler saxHandler = new SAXHandler();
			saxParser.parse(xml, saxHandler);

			planesList = saxHandler.getPlaneList();
		}catch (SAXException | ParserConfigurationException | IOException ex){
			ex.printStackTrace();
		}
		return planesList;
	}
}
