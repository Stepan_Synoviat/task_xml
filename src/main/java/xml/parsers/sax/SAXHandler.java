package xml.parsers.sax;
import xml.model.Plane;
import xml.model.Chars;
import xml.model.Parameters;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class SAXHandler extends DefaultHandler {
	private List<Plane> planesList = new ArrayList<>();
	private Plane plane;
	private Chars chars;
	private Parameters parameters;


	private boolean bModel = false;
	private boolean bOrigin = false;
	private boolean bChars = false;
	private boolean bType = false;
	private boolean bSeats = false;
	private boolean bKit = false;
	private boolean bRockets = false;
	private boolean bRadar = false;
	private boolean bPrice = false;
	private boolean bParameters = false;
	private boolean bLenght = false;
	private boolean bWeight = false;
	private boolean bHeight = false;


	public List<Plane> getPlaneList(){
		return this.planesList;
	}

	public void startElement(String uri, String localName,String qName, Attributes attributes) throws SAXException {

			Plane plane = new Plane();
			 if (qName.equalsIgnoreCase("model")){bModel = true;}
		else if (qName.equalsIgnoreCase("origin")){bOrigin = true;}
		else if (qName.equalsIgnoreCase("chars")){bChars = true;}
		else if (qName.equalsIgnoreCase("type")){bType = true;}
		else if (qName.equalsIgnoreCase("seats")){bSeats = true;}
		else if (qName.equalsIgnoreCase("kit")){bKit = true;}
		else if (qName.equalsIgnoreCase("rockets")){bRockets = true;}
		else if (qName.equalsIgnoreCase("parameters")){bParameters = true;}
		else if (qName.equalsIgnoreCase("lenght")){bLenght = true;}
		else if (qName.equalsIgnoreCase("weight")){bWeight = true;}
		else if (qName.equalsIgnoreCase("height")){bHeight = true;}
		else if (qName.equalsIgnoreCase("radar")){bRadar = true;}
		else if (qName.equalsIgnoreCase("price")){bPrice = true;}
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (qName.equalsIgnoreCase("Plane")){
			planesList.add(plane);
		}
	}

	public void characters(char ch[], int start, int length) throws SAXException {
		if (bModel){
			plane.setModel(new String(ch, start, length));
			bModel = false;
		}
		else if (bOrigin){
			plane.setOrigin(new String(ch, start, length));
			bOrigin = false;
		}
		else if (bChars){
			chars = new Chars();
			bChars = false;
		}
		else if (bType){
			String type = new String(ch, start, length);
			chars.setType(type);
			bType = false;
		}
		else if(bSeats){
			int seats = Integer.parseInt(new String(ch, start, length));
			chars.setSeats(seats);
			bSeats = false;
		}
		else if (bKit){
			boolean kit= Boolean.parseBoolean(new String(ch, start, length));
			chars.setKit(kit);
			bKit = false;
		}
		else if (bRockets){
			int rockets = Integer.parseInt(new String(ch, start, length));
			chars.setRockets(rockets);
			bRockets = false;
		}
		else if (bPrice){
			int price = Integer.parseInt(new String(ch, start, length));
			chars.setPrice(price);
			bPrice = false;
		}
		else if (bRadar){
			boolean radar = Boolean.parseBoolean(new String(ch, start, length));
			chars.setHasRadar(radar);
			bRadar = false;
		}
		else if (bParameters){
			Parameters parameters = new Parameters();
			bParameters = false;
		}
		else if (bLenght){
			double lenght = Double.parseDouble(new String(ch, start, length));
			parameters.setLength(length);
			bLenght = false;
		}
		else if (bWeight){
			double weight = Double.parseDouble(new String(ch, start, length));
			parameters.setWeight(weight);
			bWeight = false;
		}
		else if(bHeight){
			double height = Double.parseDouble(new String(ch, start, length));
			parameters.setHeight(height);
			bHeight = false;
		}
	}
}

