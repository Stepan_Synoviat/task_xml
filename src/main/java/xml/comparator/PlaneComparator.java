package xml.comparator;

import xml.model.Plane;

import java.util.Comparator;
public class PlaneComparator implements Comparator<Plane> {

	@Override
	public int compare(Plane o1, Plane o2) {
		return Double.compare(o1.getChars().getParameters().getLength(), o2.getChars().getParameters().getLength());
	}
}
